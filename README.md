# dopamine.js

dopamine.js is under development. Features and interfaces are likely to change in future.

## Description

dopamine.js is like a neurotransmitter for your javascript application.

It helps to connect software parts by messaging via Events, Callbacks, Method Calls or Plugins through an unique interface.

## Installation

Download dopamine.js and install it in your public javascripts directory.  
Include it in your document AFTER jQuery.

## Usage

See examples.html

## Dependencies

dopamine.js requires jQuery >= 1.7  
Get it from: [http://jquery.com](http://jquery.com)

## License

Licensed under the MIT license.  
See file LICENSE for details.

## Authors

dopamine.js was created and is maintained by Armin Dressler <dressler.armin at gmail.com>

Many thanks to Dirk Brünsicke for suggesting the name 'dopamine'.

## Next steps

- Include documentation into this readme
- Expose more of the native interfaces of Events and Callbacks